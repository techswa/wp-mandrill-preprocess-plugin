# README #

>Problem: You want to customize your users Mandrill transaction email experience and the WordPress plugin only has minimal support.  Mandril is the transactional email system for MailChimp.

This solution was developed to allow for the Mandrill hooks to be exploited in WordPress.  
You can select the templates based on eMember type and action.

## Custom behaviors added to Mandril via this plugin ##

* Send specific template based upon member and activity
    * New member signup
    * New sponsor signup
    * Lost password template
