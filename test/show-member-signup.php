<?php

/**
 * Display a mocked up signup email
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

//Include the template file
include '../includes/templates/membership/welcome-template.php';

 $message = array();
 $message['html']='';

 $merge_data = array();

 $merge_data['user_login'] = 'kelly12';
 $merge_data['first_name'] = 'Kelly';
 $merge_data['last_name'] = 'Robertson';
 $merge_data['user_email'] = 'kelly@email.com';


print_r(member_welcome_template($message, $merge_data));
