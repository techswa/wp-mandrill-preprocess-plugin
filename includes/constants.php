<?php


/**
 * This file contains constants that are used in SWA email
 *
 */

// Mandrill defaults

define('MANDRILL_TEST_API_KEY', 'Fb_XyWSXtDQ7iLQYUULaaA');
define('MANDRILL_ACTIVE_API_KEY', '');

/*
* eMarketing Defaults
*/

// Email types
define('NEW_SUBSCRIBER_EMAIL', 'wp_gf_new_user_notification');
define('NEW_SPONSOR_EMAIL', 'wp_gf_new_user_notification');

define('REPLACE_SUBSCRIBER_PASSWORD_EMAIL', 'wp_wp_emember_generate_and_mail_password');
define('REPLACE_SPONSOR_PASSWORD_EMAIL', 'wp_wp_emember_generate_and_mail_password');


define('NULL_EMAIL_ADDRESS', 'null@enhanceyourcreditscore.com');

// Name that recipents will see in sent emails
define('FROM_NAME_DEFAULT', 'Credit Coach');
define('FROM_NAME_CREDIT_COACH', 'Credit Coach');
define('FROM_NAME_SUPPORT', 'Support');
define('FROM_NAME_MEMBERSHIP', 'Membership');
define('FROM_NAME_SPONSOR', 'Sponsor');

// Address that recipents will see in sent emails
define('FROM_EMAIL_DEFAULT', 'CreditCoach@EnhanceYourCreditScore.com');
define('FROM_EMAIL_CREDIT_COACH', 'CreditCoach@EnhanceYourCreditScore.com');
define('FROM_EMAIL_SUPPORT', 'Support@EnhanceYourCreditScore.com');
define('FROM_EMAIL_MEMBERSHIP', 'Membership@EnhanceYourCreditScore.com');
define('FROM_EMAIL_SPONSOR', 'Sponsor@EnhanceYourCreditScore.com');
define('FROM_EMAIL_SPONSOR_SUPPORT', 'Sponsor.Support@EnhanceYourCreditScore.com');

//Message subjects - used for parsing
define('SUBJECT_REPLACE_PASSWORD', '[Enhance Your Credit Score] Notice of Password Change');

//
define('CONST_REPLACE_PASSWORD', '0');

define('TEMPLATE_REPLACE_PASSWORD', '0');

//Reply to address
// define('FROM_EMAIL_MEMBERSHIP', 'Membership@EnhanceYourCreditScore.com');
