<?php

/**
 * Apply theme to email content
 * suppression is accomplished by rerouting to reciepient named null
 *
 * @param     $message
 * @return    $message
 * @author    B Burt
 * @copyright
 */


// If message source is on blacklist then reroute to NULL
// If Mandrill rule is configured it will also catch it
// Uses the Mandril tag field containg the generating function
function swa_apply_theme($message){

  // Select and apply basic html and css
  $message = primary_theme($message);

  return $message;
}
