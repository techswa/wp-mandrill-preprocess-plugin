<?php

 /**
  * Test data.
  *
  * @param
  *
  * @author   B burt
  * @copyright
  */

 function swa_apply_template($message){

// PC::debug($message);
     //Merge data can be passed in to replace the standard payload

     $message['blog_url'] = get_bloginfo('url');
     $message['blog_name'] = get_bloginfo('name');
     $message['blog_description'] = get_bloginfo('description');

     //Get server date & time
     $date_time = new DateTime('now', new DateTimeZone('America/Chicago'));

     $message['local_date'] = $date_time->format('m/d/Y');
     $message['local_time'] = $date_time->format('g:i A');

     //Default email from
     $message['return_email'] = FROM_EMAIL_MEMBERSHIP;

     //Process based upon the current user's role

     // Get WP global for user
     global $current_user;
     //Get current user's role
     if ($current_user != ""){
     $current_user_role = swa_get_current_user_role( $current_user );
   } else {
      $current_user_role = 'member';
   }
   //For debug only - remove
   $current_user_role = 'member';
    // PC::debug('Current user role ' . $current_user_role);
    switch ($current_user_role){
    case 'administrator':
      // $message = sales_administrator_filter($message);
      break;

    case 'sales':
    // PC::debug('Routing through sales');
      $message = sponsor_email_filter($message);
      break;

    case 'silver':

      // $message = subscriber_email_filter($message);
      break;

    case 're_pro':
      // $message = re_pro_email_filter($message);
      break;

    case 'mb_pro':
    // PC::debug('Case mb_pro');
      // $message = mb_pro_email_filter($message);
      break;

    case 'member': //New subscriber - not yet signed in
          // PC::debug('Case member');
        $message = member_email_filter($message);
        break;

    default:
      // PC::debug('Case default');
      //Dont apply any filter
      $message = $message;
  }


     return $message;
 }
