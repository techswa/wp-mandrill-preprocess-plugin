<?php

/**
 * Filter and modify emails originating from the sales role
 *
 * @param     $message
 * @return    $message
 * @author
 * @copyright
 */

 function sales_email_filter($message){

   //Determine the message target based on thier role


   //Determine which email is being sent and branch
   // Retreive message source from message array
   $message_type = $message['tags']['automatic']['0'];

   $new_user_email = $message['to']['0']['email'];

   //Set the return email to sponsor support
   $message['return_email'] = FROM_EMAIL_SPONSOR_SUPPORT;

   //Get the newly created sponsor's role
   $user_info = get_user_by('email', $new_user_email);

   // Get the sponsor's ID
   $user_id = $user_info->ID;

   //Get the user object
   $user_object = new WP_User( $user_id );

   //Get the users merge_data
   $merge_data = sales_merge_data($new_user_email);

   switch ($message_type){ //Call the proper template

   case NEW_SPONSOR_EMAIL:
    //Use the appropriate template
    if ($user_object->has_cap('mb_pro')){
      $message = sales_welcome_mbpro_template($message, $merge_data);
    }

    if ($user_object->has_cap('re_pro')){
      $message = sales_welcome_repro_template($message, $merge_data);
    }

    break;

  //  case REPLACE_PASSWORD_EMAIL:
  //   //  $message = sales_email_filter($message);
  //    break;

   default:
     //Dont apply any filter
     $message = $message;
 }



   return $message;
 }
