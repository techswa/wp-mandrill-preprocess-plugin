<?php

/**
 * Query to get sale's role data for different emails
 *
 * @param     $new_user_email
 * @return    $merge_data
 * @author    B Burt
 * @copyright
 */

function sponsor_merge_data($new_user_email){
  //Get user info
  $user_info = get_user_by('email', $new_user_email);
  $merge_data = array();
  $merge_data['user_login'] = $user_info->user_login;
  $merge_data['first_name'] = $user_info->first_name;
  $merge_data['last_name'] = $user_info->last_name;
  $merge_data['user_email'] = $user_info->user_email;
  // $merge_data['user_password'] = $user_info->user_pass;
  $merge_data['return_email'] = FROM_EMAIL_SPONSOR;


return $merge_data;
}
