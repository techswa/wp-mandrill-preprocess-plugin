<?php

/**
 * Template used when a new MB Pro sponsor signs up
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

function mb_pro_welcome_template($new_user_login){

$merge_data = mb_pro_welcome_data($new_user_login);

$merge_data['return_email'] = FROM_EMAIL_SPONSOR;

$merge_data['merge_template'] = "

<div>
<div>Welcome aboard.</div>
<div>You next steps in setting up your marketing assistant are:

<ul>
<li>Complete the marketing questionaire.</li>
<li>Upload a 200px * 200px jpg or png headshot of yourself.</li>
<li>Make note of your coupon code as displayed in your dashboard.</li>
<li>Start reaching out to prospects and clients informing them of your new offering.</li>
<li>Don't forget to ask for referrals.  Almost everyone knows someone who desires a higher credit score. </li>
</ul>
<div>
<a href='http://www.enhanceyourcreditscore.com'>Setup your marketing assistant</a>
</div>
</div>

";


  return $merge_data;

}
