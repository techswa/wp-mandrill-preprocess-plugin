<?php

/**
 * Template used when a new MB Pro sponsor signs up
 *
 * @param     $message
 * @param     $merge_data
 *
 * @return    $message
 * @author
 * @copyright
 */

function mbpro_welcome_template($message, $merge_data){


//Setup merge fields
$userlogin  = $merge_data['user_login'];
$first_name = $merge_data['first_name'];
$last_name  = $merge_data['last_name'];
$email      = $merge_data['user_email'];
$password   = $merge_data['user_login'];


$message['html'] = "
<div>
<div>Welcome Mortgage Professional.</div>
<div>Your message will now be seen everytime one of your sponsored Realtor's clients reads a credit booklet. Remember
that the information is being presented in a mobile first format so that home buyers always have access.  Of course, the booklets can also be read from a tablet or desktop computer.
</div>
<ul>
<li>User name: {$userlogin}</li>
<li>First name: {$first_name}</li>
<li>Last name: {$last_name}</li>
<li>Email: {$email}</li>
<li>Starting password: {$password}</li>
</ul>
<div>Your next steps in setting up your marketing assistant are:
<ol>
<li>Change the password!  You don't want any kiddos changing your ads.</li>
<li>Complete the marketing questionaire.</li>
<li>Upload a 200px * 200px jpg or png headshot of yourself.</li>
<li>Make note of your coupon code as displayed in your dashboard.</li>
<li>Start reaching out to Realtors and get those marketing partnerships started.</li>
<li>The more Realtors you sponsor, the more people will inquire about your loans.</li>
<li>Expect a lot of referrals, almost everyone knows someone who desires a higher credit score. </li>
</ol>
<div>
<a href='http://www.enhanceyourcreditscore.com/member-login'>Configure your new marketing system</a>
</div>
<div style='font-size: 11px; color: #999999'><br/>Rev 07/19/16</div>
</div>

";

  return $message;

}
