<?php

/**
 * Filter and modify emails originating from a new subscriber signup
 *
 * @param     $message
 * @return    $message
 * @author    B burt
 * @copyright
 */

 function member_email_filter($message){

   //Determine the message target based on thier role


   //Determine which email is being sent and branch
   // Retreive message source from message array
   $message_type = $message['tags']['automatic']['0'];

   $new_user_email = $message['to']['0']['email'];

   //Set the return email to sponsor support
   $message['return_email'] = FROM_EMAIL_MEMBERSHIP;

   return $message;
 }
