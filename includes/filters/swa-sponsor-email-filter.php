<?php

/**
 * Filter and modify emails originating from a new subscriber signup
 *
 * @param     $message
 * @return    $message
 * @author
 * @copyright
 */

 function sponsor_email_filter($message){

   //Determine the message target based on thier role


   //Determine which email is being sent and branch
   // Retreive message source from message array
   $message_type = $message['tags']['automatic']['0'];

   $new_user_email = $message['to']['0']['email'];

   //Set the return email to sponsor support
   $message['return_email'] = FROM_EMAIL_MEMBERSHIP;

   //Get the users merge_data
   $merge_data = membership_merge_data($new_user_email);
   $message = membership_welcome_repro_template($message, $merge_data);

   switch ($message_type){ //Call the proper template

   case NEW_SPONSOR_EMAIL:
    //Use the appropriate template
    if ($user_object->has_cap('mb_pro')){
      $message = sales_welcome_mbpro_template($message, $merge_data);
    }

    if ($user_object->has_cap('re_pro')){
      $message = sales_welcome_repro_template($message, $merge_data);
    }

    break;

   default:
     //Dont apply any filter
     $message = $message;
 }



   return $message;
 }
