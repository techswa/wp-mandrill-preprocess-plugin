<?php


 /*
 Plugin Name: SWA WP Mandrill Preprocess
 Version: 1.0.1
 Plugin URI:
 Author: B Burt
 Author URI:
 */

 function swa_wp_mandrill_preprocess($message){

 include_once("includes/constants.php");


 include_once("includes/controllers/swa-apply-filter.php");
 include_once("includes/controllers/swa-apply-template.php");
 include_once("includes/controllers/swa-apply-theme.php");

 // include_once("includes/filters/swa-primary-email-filter.php");
 include_once("includes/filters/swa-member-email-filter.php");

 include_once("includes/themes/primary-theme.php");

 include_once("includes/templates/sponsor/mbpro/welcome-template.php");
 include_once("includes/templates/sponsor/repro/welcome-template.php");
 include_once("includes/templates/membership/welcome-template.php");

 include_once("includes/queries/sponsor/merge-data.php");
 include_once("includes/queries/member/merge-data.php");



// PC::debug('Just entered mandrill preprocess plugin');

  //Notch filter to reroute any undesired emails that are auto generated
  $message = swa_apply_filter($message);

  //Add message data defaults for template design - blog name, url, etc.
  $message = swa_apply_template($message);

  //Insert original html into the theme
  $message = swa_apply_theme($message);

  return $message;
}


add_filter( 'mandrill_payload', 'swa_mandrill_preprocess' );
